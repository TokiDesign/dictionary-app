import React from "react";

import AuthPage from "./Components/AuthPage";
import MainPage from "./Components/MainPage";
import { useUser } from "./context/user-context";
import { DictionaryProvider } from "./context/dictionary-context";

import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

function App() {
  const user = useUser();

  function PrivateRoute({ children, ...rest }) {
    return (
      <Route
        {...rest}
        render={({ location }) =>
          user ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location }
              }}
            />
          )
        }
      />
    );
  }

  return (
    <Router>
      <Switch>
        <Route path="/login">
          <AuthPage />
        </Route>

        <PrivateRoute path="/">
          <DictionaryProvider>
            <MainPage />
          </DictionaryProvider>
        </PrivateRoute>
      </Switch>
    </Router>
  );
}

export default App;
