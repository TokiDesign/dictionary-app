import React from "react";
import { useAuth } from "./auth-context";
import * as wordClient from "../clients/words-client";

const WordsStateContext = React.createContext();
const WordsDispatchContext = React.createContext();

function WordsReducer(words, action) {
  switch (action.type) {
    case "add": {
      return [...words, action.word];
    }
    case "remove": {
      return words.filter(li => li.id !== action.id);
    }
    case "update": {
      return words.map(li => {
        if (li.id === action.id) {
          return { ...li, ...action.updates };
        }

        return li;
      });
    }

    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function WordsProvider({ children }) {
  const { data } = useAuth();
  const [state, dispatch] = React.useReducer(WordsReducer, data.words);

  return (
    <WordsStateContext.Provider value={state}>
      <WordsDispatchContext.Provider value={dispatch}>
        {children}
      </WordsDispatchContext.Provider>
    </WordsStateContext.Provider>
  );
}

function removeWord(dispatch, id) {
  return wordClient.remove(id).then(data => {
    dispatch({ type: "remove", id });
    return data;
  });
}

function addWord(dispatch, listItemData) {
  return wordClient.create(listItemData).then(data => {
    dispatch({ type: "add", word: data });
    return data;
  });
}

function updateWord(dispatch, id, updates) {
  return wordClient.update(id, updates).then(data => {
    dispatch({ type: "update", id, updates });
    return data;
  });
}
function selectWord(dispatch, selected) {
  dispatch({ type: "select", selected });
}

function useWordDispatch() {
  const context = React.useContext(WordsDispatchContext);
  if (context === undefined) {
    throw new Error(`useWordDispatch must be used within a WordsProvider`);
  }
  return context;
}

function useWordState(id = null) {
  const context: any = React.useContext(WordsStateContext);

  if (context === undefined) {
    throw new Error(`useWordState must be used within a WordsProvider`);
  }
  if (id) {
    const listItem = context.filter(li => li.dictId === id);
    return listItem;
  }
  return context;
}

function useSingleWordState(dictId) {
  const words = useWordState();
  if (!words) {
    throw new Error(`useSingleWordState must be used within a WordsProvider`);
  }

  const listItem = words.find(li => li.id === dictId);
  return listItem;
}

export {
  WordsProvider,
  useWordDispatch,
  useWordState,
  useSingleWordState,
  removeWord,
  addWord,
  updateWord,
  selectWord
};
