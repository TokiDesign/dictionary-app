import React from "react";
import * as authClient from "../clients/auth-client";
import { useAsync } from "react-async";
import { bootstrapAppData } from "./bootstrap";

const AuthContext = React.createContext();

function AuthProvider(props) {
  const {
    data = { user: null, dictionaries: [], words: [] },
    reload
  } = useAsync({
    promiseFn: bootstrapAppData
  });

  const login = (username, password) => {
    authClient.login(username, password).then(reload);
  };

  const logout = () => {
    authClient.logout().then(reload);
  };

  return <AuthContext.Provider value={{ data, login, logout }} {...props} />;
}

function useAuth() {
  const context = React.useContext(AuthContext);
  if (context === undefined) {
    throw new Error(`useAuth must be used within a AuthProvider`);
  }
  return context;
}

export { AuthProvider, useAuth };
