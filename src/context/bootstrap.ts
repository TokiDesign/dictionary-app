import { getUser } from "../clients/auth-client";
import { readForUser } from "../clients/dictionary-client";
import { readForUserWords } from "../clients/words-client";
async function bootstrapAppData() {
  let data = await getUser();
  let dictionaries = await readForUser();
  let words = await readForUserWords();
  if (!data) {
    return { user: null, dictionaries: [], words: [] };
  }
  if (!dictionaries) {
    dictionaries = [];
  }
  if (!words) {
    words = [];
  }
  const { user } = data;

  return {
    user,
    dictionaries,
    words
  };
}

export { bootstrapAppData };
