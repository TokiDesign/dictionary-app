import React from "react";
import { useAuth } from "./auth-context";
import * as dictionaryClient from "../clients/dictionary-client";

const DictionaryStateContext = React.createContext();

const DictionaryDispatchContext = React.createContext();

function DictionaryReducer(dictionaries, action) {
  switch (action.type) {
    case "add": {
      return [...dictionaries, action.dictionary];
    }
    case "remove": {
      return dictionaries.filter(li => li.id !== action.id);
    }
    case "update": {
      return dictionaries.map(li => {
        if (li.id === action.id) {
          return { ...li, ...action.updates };
        }

        return li;
      });
    }

    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function DictionaryProvider({ children }) {
  const { data } = useAuth();
  const [state, dispatch] = React.useReducer(
    DictionaryReducer,
    data.dictionaries
  );

  return (
    <DictionaryStateContext.Provider value={state}>
      <DictionaryDispatchContext.Provider value={dispatch}>
        {children}
      </DictionaryDispatchContext.Provider>
    </DictionaryStateContext.Provider>
  );
}

function removeDictionary(dispatch, id) {
  return dictionaryClient.remove(id).then(data => {
    dispatch({ type: "remove", id });
    return data;
  });
}

function addDictionary(dispatch, listItemData) {
  return dictionaryClient.create(listItemData).then(data => {
    dispatch({ type: "add", dictionary: data });
    return data;
  });
}

function updateDictionary(dispatch, id, updates) {
  return dictionaryClient.update(id, updates).then(data => {
    dispatch({ type: "update", id, updates });
    return data;
  });
}
function selectDictionary(dispatch, selected) {
  dispatch({ type: "select", selected });
}

function useDictionaryDispatch() {
  const context = React.useContext(DictionaryDispatchContext);
  if (context === undefined) {
    throw new Error(
      `useDictionaryDispatch must be used within a DictionaryProvider`
    );
  }
  return context;
}

function useDictionaryState(id = null) {
  const context = React.useContext(DictionaryStateContext);

  if (context === undefined) {
    throw new Error(
      `useDictionaryState must be used within a DictionaryProvider`
    );
  }

  if (id) {
    const items = context.filter(li => li.userId === id);
    return items;
  }

  return context;
}

function useSingleDictionaryState(dictId) {
  const dictionaries: any = useDictionaryState();
  if (!dictionaries) {
    throw new Error(
      `useSingleDictionaryState must be used within a DictionaryProvider`
    );
  }

  const listItem = dictionaries.find(li => li.id === dictId);
  return listItem;
}

export {
  DictionaryProvider,
  useDictionaryDispatch,
  useDictionaryState,
  useSingleDictionaryState,
  removeDictionary,
  addDictionary,
  updateDictionary,
  selectDictionary,
  DictionaryStateContext
};
