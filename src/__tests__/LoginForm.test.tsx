import React from "react";
import { render, fireEvent, waitForElement } from "@testing-library/react";
import { Router } from "react-router-dom";
import LoginForm from "../Components/AuthPage";
import { createMemoryHistory } from "history";
import { AuthProvider } from "../context/auth-context";
import { UserProvider } from "../context/user-context";
function renderForm(props = {}) {
  const defaultProps = {
    dicts: [],
    openDialog: () => {}
  };

  const history = createMemoryHistory({
    initialEntries: ["/starting/point"]
  });
  return render(
    <AuthProvider>
      <UserProvider>
        <Router history={history}>
          <LoginForm {...defaultProps} {...props} />
        </Router>
      </UserProvider>
    </AuthProvider>
  );
}

describe("<LoginForm />", () => {
  test("should display a blank login form, with remember me checked by default", async () => {
    const { findByTestId } = renderForm();

    const form = await findByTestId("login-form");
    expect(form).not.toBeNull();
  });
});
