import React from "react";
import {
  render as rtlRender,
  fireEvent,
  waitForElement
} from "@testing-library/react";
import { DictionaryProvider } from "../context/dictionary-context";
import { AuthProvider } from "../context/auth-context";

function render(
  ui,
  {
    store = { dicts: [{ name: "test", from: "en", to: "sk", id: "1" }] },
    ...options
  } = {}
) {
  function Wrapper(props) {
    return (
      <AuthProvider>
        <DictionaryProvider value={store} {...props} />
      </AuthProvider>
    );
  }
  return rtlRender(ui, { wrapper: Wrapper, ...options });
}

import DictionaryList from "../Components/DictionaryList";

function renderDictionaryList(props = {}) {
  const defaultProps = {
    dicts: [],
    openDialog: () => {}
  };
  return render(<DictionaryList {...defaultProps} {...props} />);
}

describe("<DictionaryList />", () => {
  test("should be rendered", async () => {
    const { findByTestId } = renderDictionaryList();

    const dictionaryList = await findByTestId("dictionary-list");
    expect(dictionaryList).not.toBeNull();
  });
});
