function create(dictData) {
  let newdict = [dictData];
  let dicts: any = window.localStorage.getItem("dictionaries");
  if (dicts) {
    dicts = JSON.parse(dicts);
    newdict = [...dicts, dictData];
  }

  window.localStorage.setItem("dictionaries", JSON.stringify(newdict));
  return Promise.resolve(dictData);
}

function update(id, updates) {
  let dicts: any = window.localStorage.getItem("dictionaries");
  if (dicts) {
    dicts = JSON.parse(dicts);
    const newDicts = dicts.map(li => {
      if (li.id === id) {
        return { ...li, ...updates };
      }
      return li;
    });
    window.localStorage.setItem("dictionaries", JSON.stringify(newDicts));
  }
  return Promise.resolve(id);
}

function readForUser() {
  let dicts: any = window.localStorage.getItem("dictionaries");
  if (dicts) {
    dicts = JSON.parse(dicts);
  }

  return Promise.resolve(dicts);
}

function remove(id) {
  let dicts: any = window.localStorage.getItem("dictionaries");
  if (dicts) {
    dicts = JSON.parse(dicts);
    let newdict = dicts.filter(li => li.id !== id);

    window.localStorage.setItem("dictionaries", JSON.stringify(newdict));
  }
  return Promise.resolve(id);
}

export { create, remove, readForUser, update };
