async function postData(url = "", data = {}) {
  const response = await fetch(url, {
    method: "POST",
    body: JSON.stringify(data)
  });
  return await response.json();
}

const voiceApi = (text, selectedDict) =>
  postData(
    `${process.env.REACT_APP_TEXT_TO_SPEECH_URL}?key=${process.env.REACT_APP_TEXT_TO_SPEECH_KEY}`,
    {
      audioConfig: {
        audioEncoding: "MP3"
      },
      input: {
        text
      },
      voice: {
        languageCode: selectedDict.from
      }
    }
  );

function translationApi(selectedDict, name) {
  return postData(
    `${process.env.REACT_APP_TRANSLATION_URL}?key=${process.env.REACT_APP_TRANSLATION_API_KEY}&target=${selectedDict.to}&q=${name}`
  );
}

export { translationApi, voiceApi };
