function create(data) {
  let newWords = [data];
  let words: any = window.localStorage.getItem("words");
  if (words) {
    words = JSON.parse(words);
    newWords = [...words, data];
  }

  window.localStorage.setItem("words", JSON.stringify(newWords));
  return Promise.resolve(data);
}

function update(id, updates) {
  let words: any = window.localStorage.getItem("words");
  if (words) {
    words = JSON.parse(words);
    const newWords = words.map(li => {
      if (li.id === id) {
        return { ...li, ...updates };
      }
      return li;
    });
    window.localStorage.setItem("words", JSON.stringify(newWords));
  }
  return Promise.resolve(id);
}

function readForUserWords() {
  let words: any = window.localStorage.getItem("words");
  if (words) {
    words = JSON.parse(words);
  }

  return Promise.resolve(words);
}

function remove(id) {
  let words: any = window.localStorage.getItem("words");
  if (words) {
    words = JSON.parse(words);
    let newwords = words.filter(li => li.id !== id);

    window.localStorage.setItem("words", JSON.stringify(newwords));
  }
  return Promise.resolve(id);
}

export { create, remove, readForUserWords, update };
