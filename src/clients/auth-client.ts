function getUser() {
  let user: any = window.localStorage.getItem("userLogedIn");
  return Promise.resolve({ user: JSON.parse(user) });
}

function login(username, password) {
  window.localStorage.setItem(
    "userLogedIn",
    JSON.stringify({ username, password })
  );
  return Promise.resolve({ username, password });
}

function logout() {
  window.localStorage.removeItem("userLogedIn");
  return Promise.resolve();
}

export { login, logout, getUser };
