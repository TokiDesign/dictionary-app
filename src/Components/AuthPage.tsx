import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import CardHeader from "@material-ui/core/CardHeader";
import { useAuth } from "../context/auth-context";
import { withRouter } from "react-router-dom";
import { useUser } from "../context/user-context";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      flexWrap: "wrap",
      width: 400,
      margin: `${theme.spacing(0)} auto`
    },
    loginBtn: {
      border: 0,
      borderRadius: 3,
      background: "linear-gradient(45deg, #4dcac4 30%, #3796b3 90%)",
      boxShadow: "0 3px 5px 2px rgba(97, 243, 251, .3)",
      color: "white",
      height: 48,
      padding: "0 30px",
      marginTop: theme.spacing(2),
      flexGrow: 1
    },
    header: {
      textAlign: "center",
      background: "#1f213b",
      color: "#fff"
    },
    card: {
      marginTop: theme.spacing(10)
    }
  })
);

export interface Props {
  history: boolean;
}

const AuthPage = ({ history, location }) => {
  const classes = useStyles();
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [isButtonDisabled, setIsButtonDisabled] = useState<boolean>(true);
  const [helperText, setHelperText] = useState<string>("");

  const [error, setError] = useState<boolean>(false);

  const { login } = useAuth();
  const user = useUser();

  useEffect(() => {
    if (username.trim() && password.trim()) {
      setIsButtonDisabled(false);
    } else {
      setIsButtonDisabled(true);
    }
  }, [username, password]);

  useEffect(() => {
    if (user) {
      if (location.state && location.state.from) {
        history.push(location.state.from.pathname);
      } else {
        history.push("/dictionaries");
      }
    }
  }, [user, history, location]);

  const handleLogin = () => {
    if (username === "" && password === "") {
      setError(true);
      setHelperText("Incorrect username or password");
    } else {
      setError(false);
      setHelperText("Login Successfully");
      login(username, password);
      history.push("/dictionaries");
    }
  };

  const handleKeyPress = (e: any) => {
    if (e.keyCode === 13 || e.which === 13) {
      isButtonDisabled || handleLogin();
    }
  };

  return (
    <React.Fragment>
      <form
        data-testid="login-form"
        className={classes.container}
        noValidate
        autoComplete="off"
      >
        <Card className={classes.card}>
          <CardHeader className={classes.header} title="Login" />
          <CardContent>
            <div>
              <TextField
                error={error}
                fullWidth
                id="username"
                type="email"
                label="Username"
                placeholder="Username"
                margin="normal"
                onChange={e => setUsername(e.target.value)}
                onKeyPress={e => handleKeyPress(e)}
              />
              <TextField
                error={error}
                fullWidth
                id="password"
                type="password"
                label="Password"
                placeholder="Password"
                margin="normal"
                helperText={helperText}
                onChange={e => setPassword(e.target.value)}
                onKeyPress={e => handleKeyPress(e)}
              />
            </div>
          </CardContent>
          <CardActions>
            <Button
              variant="contained"
              size="large"
              color="secondary"
              className={classes.loginBtn}
              onClick={() => handleLogin()}
              disabled={isButtonDisabled}
            >
              Login
            </Button>
          </CardActions>
        </Card>
      </form>
    </React.Fragment>
  );
};

export default withRouter(AuthPage);
