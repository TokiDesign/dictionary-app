import React, { useState, useEffect } from "react";

import languages from "../ISO-639-1-language.json";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Dialog from "@material-ui/core/Dialog";
import Button from "@material-ui/core/Button";
import { useUser } from "../context/user-context";
import {
  addDictionary,
  useDictionaryDispatch,
  updateDictionary
} from "../context/dictionary-context";

import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";

import { useHistory, withRouter } from "react-router-dom";

const shortid = require("shortid");
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      flexWrap: "wrap",
      width: "100%",
      justifyContent: "center",
      marginTop: "30px"
    },
    dialogContainer: {
      display: "flex",
      flexWrap: "wrap",
      width: "100%",
      justifyContent: "center",
      marginTop: "30px",
      flexDirection: "column",
      minWidth: "300px"
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    button: {
      background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
      border: 0,
      borderRadius: 3,
      boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
      color: "white",
      height: 48,
      padding: "0 30px",
      margin: "20px"
    },
    text: {
      color: "white"
    }
  })
);

function DictionaryDialog({ location, edited, setOpen, open }) {
  const [from, setFrom] = useState<string>("");
  const [to, setTo] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [editId, setEditID] = useState<string>("");
  const [disabled, setDisabled] = useState<boolean>(true);

  const classes = useStyles();
  const user: any = useUser();

  const dispatch = useDictionaryDispatch();

  useEffect(() => {
    if (from && to && name) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [from, to, name]);

  useEffect(() => {
    setDisabled(true);
  }, [location]);

  useEffect(() => {
    if (edited && edited.id) {
      setEditID(edited.id);
      setName(edited.name);
      setFrom(edited.from);
      setTo(edited.to);
    }
  }, [edited]);

  const selectFromLanguage = event => {
    setFrom(event.target.value || "");
  };
  const selectToLanguage = event => {
    setTo(event.target.value || "");
  };

  const addDictName = event => {
    setName(event.target.value || "");
  };

  const handleClose = () => {
    setOpen(false);
    setName("");
    setTo("");
    setFrom("");
    setEditID("");
  };

  let history = useHistory();
  const createNewDictionary = () => {
    const id = shortid.generate();
    const data = {
      name,
      from,
      to,
      id,
      userId: user.username
    };
    addDictionary(dispatch, data);

    handleClose();

    history.push(`/dictionaries/${id}`);
  };

  const update = () => {
    const data = {
      name,
      from,
      to,
      id: editId,
      userId: user.username
    };
    updateDictionary(dispatch, editId, data);
    handleClose();
  };

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      open={open}
      onClose={handleClose}
    >
      <DialogTitle>
        {editId ? "Edit dictionary" : "Add new dictionary"}
      </DialogTitle>
      <DialogContent>
        <form className={classes.dialogContainer}>
          <FormControl className={classes.formControl}>
            <TextField
              value={name}
              id="standard-basic"
              onChange={addDictName}
              label="Dictionary name"
            />
          </FormControl>

          <FormControl className={classes.formControl}>
            <InputLabel>Translate from</InputLabel>
            <Select
              value={from}
              onChange={selectFromLanguage}
              input={<Input />}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {languages.map(i => (
                <MenuItem key={i.value} value={i.value}>
                  {i.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

          <FormControl className={classes.formControl}>
            <InputLabel id="demo-dialog-select-label">Translate to</InputLabel>
            <Select
              labelId="demo-dialog-select-label"
              id="demo-dialog-select"
              value={to}
              onChange={selectToLanguage}
              input={<Input />}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {languages.map(i => (
                <MenuItem key={i.value} value={i.value}>
                  {i.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        {editId ? (
          <Button onClick={update} disabled={disabled} color="primary">
            Update
          </Button>
        ) : (
          <Button
            className={classes.button}
            onClick={createNewDictionary}
            disabled={disabled}
          >
            Add
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
}

export default withRouter(DictionaryDialog);
