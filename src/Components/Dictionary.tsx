import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import HelpIcon from "@material-ui/icons/Help";
import Words from "./Words";
import { useSingleDictionaryState } from "../context/dictionary-context";
import { addWord, useWordDispatch, updateWord } from "../context/words-context";
import { useParams, withRouter } from "react-router-dom";
import { translationApi } from "../clients/translation-client";
const shortid = require("shortid");

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: "absolute",
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: "2px solid #000",
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3)
    },
    root: {
      "& > *": {
        margin: theme.spacing(1),
        width: 200
      }
    },
    container: {
      display: "flex",
      flexWrap: "wrap",
      flexDirection: "column",
      minWidth: "360px"
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    hint: {
      margin: "10px 0"
    },
    button: {
      marginTop: "20px",
      background: "linear-gradient(45deg, #4dcac4 30%, #3796b3 90%)",
      border: 0,
      borderRadius: 3,
      boxShadow: "0 3px 5px 2px rgba(97, 243, 251, .3)",
      color: "white",
      height: 48,
      padding: "0 30px"
    },
    buttonClose: {
      marginTop: "20px",

      border: 0,
      borderRadius: 3,

      background: "linear-gradient(45deg, #c3c3c3 30%, #8c8c8c 90%)",
      boxShadow: "0 3px 5px 2px rgba(179, 179, 179, 0.3)",
      color: "white",
      height: 48,
      padding: "0 30px"
    }
  })
);

const Dictionary = () => {
  const classes = useStyles();

  const [disabled, setDisabled] = useState<boolean>(true);
  const [open, setOpen] = useState<boolean>(false);
  const [name, setName] = useState<string>("");
  const [translation, seTranslation] = useState<string>("");
  const [editId, setEditID] = useState<string>("");

  useEffect(() => {
    if (translation && name) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [translation, name]);

  let { id } = useParams();
  let selectedDict = useSingleDictionaryState(id);

  const dispatch = useWordDispatch();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setName("");
    seTranslation("");
    setEditID("");
  };
  const addWordName = event => {
    setName(event.target.value || "");
  };

  const addTranslation = event => {
    seTranslation(event.target.value || "");
  };

  const editWord = edit => {
    setEditID(edit.id);
    setOpen(true);
    setName(edit.name);
    seTranslation(edit.translation);
  };
  const createNewDict = () => {
    const data = {
      name,
      translation,
      id: shortid.generate(),
      dictId: id
    };
    addWord(dispatch, data);
    handleClose();
  };

  const updateDict = () => {
    const data = {
      name,
      translation,
      id: editId,
      dictId: id
    };
    updateWord(dispatch, editId, data);
    handleClose();
  };

  const hint = name => {
    translationApi(selectedDict, name).then(res => {
      if (res.data) {
        const result = res.data.translations.reduce(function(t, val, index) {
          const comma = t.length ? ", " : "";
          return t + comma + val.translatedText;
        }, translation);

        seTranslation(result);
      } else {
        seTranslation("no result");
      }
    });
  };

  return (
    <div>
      <Button className={classes.button} onClick={handleClickOpen}>
        Add Word
      </Button>
      {selectedDict && (
        <Words selectedDict={selectedDict} openDialog={editWord} />
      )}

      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        open={open}
        onClose={handleClose}
      >
        <DialogTitle>{editId ? "Edit word" : "Add new word"}</DialogTitle>
        <DialogContent>
          <form className={classes.container}>
            <TextField
              value={name}
              id="standard-basic"
              onChange={addWordName}
              label="Name"
            />

            <HelpIcon
              className={classes.hint}
              onClick={() => {
                hint(name);
              }}
            />

            <TextField
              value={translation}
              multiline
              rows="4"
              id="standard-basic"
              onChange={addTranslation}
              label="Translation"
            />
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} className={classes.buttonClose}>
            Cancel
          </Button>
          {editId ? (
            <Button
              className={classes.button}
              disabled={disabled}
              onClick={updateDict}
            >
              Update
            </Button>
          ) : (
            <Button
              onClick={createNewDict}
              disabled={disabled}
              className={classes.button}
            >
              Add
            </Button>
          )}
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default withRouter(Dictionary);
