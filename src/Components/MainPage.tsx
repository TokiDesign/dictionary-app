import React, { useState } from "react";
import { useAuth } from "../context/auth-context";
import { useUser } from "../context/user-context";
import clsx from "clsx";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import DictionaryDialog from "./DictionaryDialog";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Container from "@material-ui/core/Container";
import { useDictionaryState } from "../context/dictionary-context";
import { WordsProvider } from "../context/words-context";
import DictionaryList from "./DictionaryList";
import Dictionary from "./Dictionary";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter
} from "react-router-dom";

const drawerWidth = 240;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      paddingRight: 24,
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between"
    },
    appBarSpacer: theme.mixins.toolbar,
    toolbarIcon: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: "0 8px",
      ...theme.mixins.toolbar
    },
    appBar: {
      backgroundColor: "#1f213b",
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },

    paper: {
      padding: theme.spacing(2),
      display: "flex",
      overflow: "auto",
      flexDirection: "column"
    },
    root: {
      display: "flex"
    },
    container: {
      display: "flex",
      flexWrap: "wrap",
      width: "100%",
      justifyContent: "center",
      marginTop: "30px"
    },

    content: {
      flexGrow: 1,
      height: "100vh",
      overflow: "auto",
      background: "#f3f3f3"
    },

    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    drawerPaper: {
      position: "relative",
      whiteSpace: "nowrap",
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    drawerPaperClose: {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9)
      }
    },
    button: {
      background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
      border: 0,
      borderRadius: 3,
      boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
      color: "white",
      height: 48,
      padding: "0 30px",
      margin: "20px"
    },
    text: {
      color: "white"
    },
    leftPart: {
      color: "white",
      float: "right",
      display: "flex",
      justifyContent: "right"
    },
    logout: {
      color: "#ff895c"
    }
  })
);

interface Dictionary {
  to: string;
  from: string;
  name: string;
  userId: string;
  id: string;
}

function MainPage() {
  const { logout } = useAuth();
  const user: any = useUser();
  const classes = useStyles();

  const [open, setOpen] = useState<boolean>(false);
  const [edited, setEdited] = useState<Dictionary | null>(null);

  const dicts: Dictionary[] = useDictionaryState(user.username);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const editDictionary = edit => {
    setEdited(edit);
    setOpen(true);
  };

  return (
    <Router>
      <div className={classes.root}>
        <AppBar position="absolute" className={clsx(classes.appBar)}>
          <Toolbar className={classes.toolbar}>
            <Typography className={classes.text} variant="h4">
              Dictionary app
            </Typography>
            <div className={classes.leftPart}>
              <Typography className={classes.text} variant="h6">
                {user.username}
              </Typography>
              <Button className={classes.logout} onClick={logout}>
                Logout
              </Button>
            </div>
          </Toolbar>
        </AppBar>

        <Drawer
          variant="permanent"
          classes={{
            paper: clsx(classes.drawerPaper)
          }}
          open
        >
          <div className={classes.appBarSpacer} />
          <Button className={classes.button} onClick={handleClickOpen}>
            Create Dictionary
          </Button>
          <hr />
          <List>
            {dicts && dicts.length ? (
              <DictionaryList openDialog={editDictionary} dicts={dicts} />
            ) : null}
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Container maxWidth="lg" className={classes.container}>
            <Switch>
              <Route exact path={`/dictionaries`}>
                <h2>Please select a dictionary or create one.</h2>
              </Route>
              <Route path={`/dictionaries/:id`}>
                <WordsProvider>
                  <Dictionary />
                </WordsProvider>
              </Route>
            </Switch>
            <DictionaryDialog open={open} edited={edited} setOpen={setOpen} />
          </Container>
        </main>
      </div>
    </Router>
  );
}

export default withRouter(MainPage);
