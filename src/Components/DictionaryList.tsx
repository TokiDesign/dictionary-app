import React from "react";
import { Link } from "react-router-dom";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import ListItem from "@material-ui/core/ListItem";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import {
  removeDictionary,
  useDictionaryDispatch
} from "../context/dictionary-context";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    link: {
      color: "black",
      display: "flex",
      justifyContent: "space-between",
      width: "100%",
      textDecoration: "none"
    }
  })
);

const DictionaryList = ({ dicts, openDialog }) => {
  const dispatch = useDictionaryDispatch();
  const classes = useStyles();
  return (
    <div data-testid="dictionary-list">
      {dicts.map(i => (
        <ListItem data-testid="list-item" key={i.id} button>
          <Link className={classes.link} to={"/dictionaries/" + i.id}>
            {i.name} ({i.from}-{i.to})
            <div>
              <EditIcon onClick={() => openDialog(i)} />{" "}
              <DeleteIcon onClick={() => removeDictionary(dispatch, i.id)} />
            </div>
          </Link>
        </ListItem>
      ))}
    </div>
  );
};

export default DictionaryList;
